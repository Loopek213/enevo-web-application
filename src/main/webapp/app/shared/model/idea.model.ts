import { IUser } from 'app/shared/model/user.model';
import { ICategory } from 'app/shared/model/category.model';

export interface IIdea {
  id?: number;
  ideaId?: string;
  ideaText?: string;
  userName?: string;
  categoryName?: string;
  userId?: IUser;
  category?: ICategory;
}

export const defaultValue: Readonly<IIdea> = {};
