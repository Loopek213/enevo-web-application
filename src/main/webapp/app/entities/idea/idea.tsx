import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAllAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './idea.reducer';
import { IIdea } from 'app/shared/model/idea.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IIdeaProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export class Idea extends React.Component<IIdeaProps> {
  componentDidMount() {
    this.props.getEntities();
  }

  render() {
    const { ideaList, match } = this.props;
    return (
      <div>
        <h2 id="idea-heading">
          Ideas
          <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
            <FontAwesomeIcon icon="plus" />
            &nbsp; Create a new Idea
          </Link>
        </h2>
        <div className="table-responsive">
          {ideaList && ideaList.length > 0 ? (
            <Table responsive>
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Idea Id</th>
                  <th>Idea Text</th>
                  <th>User Name</th>
                  <th>Category Name</th>
                  <th>User Id</th>
                  <th>Category</th>
                  <th />
                </tr>
              </thead>
              <tbody>
                {ideaList.map((idea, i) => (
                  <tr key={`entity-${i}`}>
                    <td>
                      <Button tag={Link} to={`${match.url}/${idea.id}`} color="link" size="sm">
                        {idea.id}
                      </Button>
                    </td>
                    <td>{idea.ideaId}</td>
                    <td>{idea.ideaText}</td>
                    <td>{idea.userName}</td>
                    <td>{idea.categoryName}</td>
                    <td>{idea.userId ? idea.userId.id : ''}</td>
                    <td>{idea.category ? <Link to={`category/${idea.category.id}`}>{idea.category.id}</Link> : ''}</td>
                    <td className="text-right">
                      <div className="btn-group flex-btn-group-container">
                        <Button tag={Link} to={`${match.url}/${idea.id}`} color="info" size="sm">
                          <FontAwesomeIcon icon="eye" /> <span className="d-none d-md-inline">View</span>
                        </Button>
                        <Button tag={Link} to={`${match.url}/${idea.id}/edit`} color="primary" size="sm">
                          <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
                        </Button>
                        <Button tag={Link} to={`${match.url}/${idea.id}/delete`} color="danger" size="sm">
                          <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Delete</span>
                        </Button>
                      </div>
                    </td>
                  </tr>
                ))}
              </tbody>
            </Table>
          ) : (
            <div className="alert alert-warning">No Ideas found</div>
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ idea }: IRootState) => ({
  ideaList: idea.entities
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Idea);
