import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './idea.reducer';
import { IIdea } from 'app/shared/model/idea.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IIdeaDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export class IdeaDetail extends React.Component<IIdeaDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { ideaEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            Idea [<b>{ideaEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="ideaId">Idea Id</span>
            </dt>
            <dd>{ideaEntity.ideaId}</dd>
            <dt>
              <span id="ideaText">Idea Text</span>
            </dt>
            <dd>{ideaEntity.ideaText}</dd>
            <dt>
              <span id="userName">User Name</span>
            </dt>
            <dd>{ideaEntity.userName}</dd>
            <dt>
              <span id="categoryName">Category Name</span>
            </dt>
            <dd>{ideaEntity.categoryName}</dd>
            <dt>User Id</dt>
            <dd>{ideaEntity.userId ? ideaEntity.userId.id : ''}</dd>
            <dt>Category</dt>
            <dd>{ideaEntity.category ? ideaEntity.category.id : ''}</dd>
          </dl>
          <Button tag={Link} to="/entity/idea" replace color="info">
            <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
          </Button>
          &nbsp;
          <Button tag={Link} to={`/entity/idea/${ideaEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ idea }: IRootState) => ({
  ideaEntity: idea.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(IdeaDetail);
