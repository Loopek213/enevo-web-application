import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { IIdea, defaultValue } from 'app/shared/model/idea.model';

export const ACTION_TYPES = {
  FETCH_IDEA_LIST: 'idea/FETCH_IDEA_LIST',
  FETCH_IDEA: 'idea/FETCH_IDEA',
  CREATE_IDEA: 'idea/CREATE_IDEA',
  UPDATE_IDEA: 'idea/UPDATE_IDEA',
  DELETE_IDEA: 'idea/DELETE_IDEA',
  RESET: 'idea/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IIdea>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false
};

export type IdeaState = Readonly<typeof initialState>;

// Reducer

export default (state: IdeaState = initialState, action): IdeaState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_IDEA_LIST):
    case REQUEST(ACTION_TYPES.FETCH_IDEA):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_IDEA):
    case REQUEST(ACTION_TYPES.UPDATE_IDEA):
    case REQUEST(ACTION_TYPES.DELETE_IDEA):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_IDEA_LIST):
    case FAILURE(ACTION_TYPES.FETCH_IDEA):
    case FAILURE(ACTION_TYPES.CREATE_IDEA):
    case FAILURE(ACTION_TYPES.UPDATE_IDEA):
    case FAILURE(ACTION_TYPES.DELETE_IDEA):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_IDEA_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_IDEA):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_IDEA):
    case SUCCESS(ACTION_TYPES.UPDATE_IDEA):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_IDEA):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = 'api/ideas';

// Actions

export const getEntities: ICrudGetAllAction<IIdea> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_IDEA_LIST,
  payload: axios.get<IIdea>(`${apiUrl}?cacheBuster=${new Date().getTime()}`)
});

export const getEntity: ICrudGetAction<IIdea> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_IDEA,
    payload: axios.get<IIdea>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IIdea> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_IDEA,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IIdea> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_IDEA,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IIdea> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_IDEA,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
