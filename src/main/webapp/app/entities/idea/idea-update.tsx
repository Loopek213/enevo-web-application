import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { IUser } from 'app/shared/model/user.model';
import { getUsers } from 'app/modules/administration/user-management/user-management.reducer';
import { ICategory } from 'app/shared/model/category.model';
import { getEntities as getCategories } from 'app/entities/category/category.reducer';
import { getEntity, updateEntity, createEntity, reset } from './idea.reducer';
import { IIdea } from 'app/shared/model/idea.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer, convertDateTimeToServer } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface IIdeaUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export interface IIdeaUpdateState {
  isNew: boolean;
  userIdId: string;
  categoryId: string;
}

export class IdeaUpdate extends React.Component<IIdeaUpdateProps, IIdeaUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      userIdId: '0',
      categoryId: '0',
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextProps.updateSuccess !== this.props.updateSuccess && nextProps.updateSuccess) {
      this.handleClose();
    }
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }

    this.props.getUsers();
    this.props.getCategories();
  }

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { ideaEntity } = this.props;
      const entity = {
        ...ideaEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/idea');
  };

  render() {
    const { ideaEntity, users, categories, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="enEvoWebApplicationApp.idea.home.createOrEditLabel">Create or edit a Idea</h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : ideaEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="idea-id">ID</Label>
                    <AvInput id="idea-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="ideaIdLabel" for="idea-ideaId">
                    Idea Id
                  </Label>
                  <AvField id="idea-ideaId" type="text" name="ideaId" />
                </AvGroup>
                <AvGroup>
                  <Label id="ideaTextLabel" for="idea-ideaText">
                    Idea Text
                  </Label>
                  <AvField id="idea-ideaText" type="text" name="ideaText" />
                </AvGroup>
                <AvGroup>
                  <Label id="userNameLabel" for="idea-userName">
                    User Name
                  </Label>
                  <AvField id="idea-userName" type="text" name="userName" />
                </AvGroup>
                <AvGroup>
                  <Label id="categoryNameLabel" for="idea-categoryName">
                    Category Name
                  </Label>
                  <AvField id="idea-categoryName" type="text" name="categoryName" />
                </AvGroup>
                <AvGroup>
                  <Label for="idea-userId">User Id</Label>
                  <AvInput id="idea-userId" type="select" className="form-control" name="userId.id">
                    <option value="" key="0" />
                    {users
                      ? users.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.id}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <AvGroup>
                  <Label for="idea-category">Category</Label>
                  <AvInput id="idea-category" type="select" className="form-control" name="category.id">
                    <option value="" key="0" />
                    {categories
                      ? categories.map(otherEntity => (
                          <option value={otherEntity.id} key={otherEntity.id}>
                            {otherEntity.id}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/idea" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />
                  &nbsp;
                  <span className="d-none d-md-inline">Back</span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                  <FontAwesomeIcon icon="save" />
                  &nbsp; Save
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  users: storeState.userManagement.users,
  categories: storeState.category.entities,
  ideaEntity: storeState.idea.entity,
  loading: storeState.idea.loading,
  updating: storeState.idea.updating,
  updateSuccess: storeState.idea.updateSuccess
});

const mapDispatchToProps = {
  getUsers,
  getCategories,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(IdeaUpdate);
