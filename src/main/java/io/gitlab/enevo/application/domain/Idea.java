package io.gitlab.enevo.application.domain;
import io.swagger.annotations.ApiModel;

import javax.persistence.*;

import java.io.Serializable;

/**
 * enEvo Klassen.
 * @author mmakkai
 */
@ApiModel(description = "enEvo Klassen. @author mmakkai")
@Entity
@Table(name = "idea")
public class Idea implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "idea_id")
    private String ideaId;

    @Column(name = "idea_text")
    private String ideaText;

    @Column(name = "user_name")
    private String userName;

    @Column(name = "category_name")
    private String categoryName;

    @OneToOne
    @JoinColumn(unique = true)
    private User userId;

    @OneToOne
    @JoinColumn(unique = true)
    private Category category;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdeaId() {
        return ideaId;
    }

    public Idea ideaId(String ideaId) {
        this.ideaId = ideaId;
        return this;
    }

    public void setIdeaId(String ideaId) {
        this.ideaId = ideaId;
    }

    public String getIdeaText() {
        return ideaText;
    }

    public Idea ideaText(String ideaText) {
        this.ideaText = ideaText;
        return this;
    }

    public void setIdeaText(String ideaText) {
        this.ideaText = ideaText;
    }

    public String getUserName() {
        return userName;
    }

    public Idea userName(String userName) {
        this.userName = userName;
        return this;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public Idea categoryName(String categoryName) {
        this.categoryName = categoryName;
        return this;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public User getUserId() {
        return userId;
    }

    public Idea userId(User user) {
        this.userId = user;
        return this;
    }

    public void setUserId(User user) {
        this.userId = user;
    }

    public Category getCategory() {
        return category;
    }

    public Idea category(Category category) {
        this.category = category;
        return this;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Idea)) {
            return false;
        }
        return id != null && id.equals(((Idea) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Idea{" +
            "id=" + getId() +
            ", ideaId='" + getIdeaId() + "'" +
            ", ideaText='" + getIdeaText() + "'" +
            ", userName='" + getUserName() + "'" +
            ", categoryName='" + getCategoryName() + "'" +
            "}";
    }
}
