/**
 * View Models used by Spring MVC REST controllers.
 */
package io.gitlab.enevo.application.web.rest.vm;
