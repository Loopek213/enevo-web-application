/**
 * Data Access Objects used by WebSocket services.
 */
package io.gitlab.enevo.application.web.websocket.dto;
