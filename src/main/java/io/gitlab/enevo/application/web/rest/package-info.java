/**
 * Spring MVC REST controllers.
 */
package io.gitlab.enevo.application.web.rest;
