/**
 * MapStruct mappers for mapping domain objects and Data Transfer Objects.
 */
package io.gitlab.enevo.application.service.mapper;
